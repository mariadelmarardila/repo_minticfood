from rest_framework import serializers
from autApp.models.user import User



class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'email','password', 'nombre','edad','direccion','telefono','ciudad','grupo','fecha_reg']

    def create(self, validated_data):
        accountData = validated_data
        userInstance = User.objects.create(**validated_data)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        return {
            'id': user.id,
            'email': user.email,
            'nombre': user.nombre,
            'edad': user.edad,
            'direccion': user.direccion,
            'telfono': user.telefono,
            'ciudad': user.ciudad,
            'grupo': user.grupo,
            'fecha_reg': user.fecha_reg
            

        }
        
